# cidenetChallenge

## Name
cidenetChallenge

## Description
Java application that attends REST type requests to be able to perform actions on employees which are persisted in some repository. These actions can be create, edit or filter information, following in all cases different business rules.

## Installation
Database: This application uses a MySQL relational database, but it can be any type of relational database. For the correct installation you only need to create the database. To do this, execute the following query command in the database context.
```
CREATE DATABASE `cidenet`
```
Once this is done, in application.properties you will find the necessary attributes to connect to the database. Here is a sample, but you must replace them with the ones found in your own database.
```
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/cidenet
spring.datasource.username=root
spring.datasource.password=root
```
Make sure that the ip address and port are correct. /cidenet is the name of the database we just created so it should not be modified.
Finally check the username and password for the database. These can be found in the application you are using to create the database.

Java Application: The application uses the Springboot framework and the maven compiler. Therefore, before we start we are going to compile the maven project and check that everything works correctly. From a terminal enter the following command:
```
mvn clean install
```
The expected result should be BUILD SUCCESS

In case your IDE is not properly configured to use maven please read the following command for IntelliJ:

https://www.jetbrains.com/help/idea/convert-a-regular-project-into-a-maven-project.html#:~:text=Add%20Maven%20support%EF%BB%B%BF&text=In%20the%20Project%20tool%20window,layout%20in%20in%20Project%20tool%20window.

Once this is done, it is time to run the application. 

There are several ways to do this. The first is to go to the ChallengeApplication class and click on the green arrow on the left that says 'run'.
The second is through the terminal which is by running the following command:
```
mvn spring-boot:run
```
It will take a few seconds to get up and once it is done, it will tell us on which ip and port it did it. Generally it is http://localhost:8080/

And that's all for the installation!

## Usage
To use the application, we will obviously need a client to be able to make the requests. Although you can use the web server, we recommend applications such as Postman.

From there we will be able to consume and post all the information we want by using the controllers.
Some examples are:

http://localhost:8080/get-all-users

http://localhost:8080/get-an-user/FIRSTNAME.LASTNAME@cidenet.com.us

http://localhost:8080/filter?lastName=LASTNAME&state=ACTIVE

The basic structure of the json to request a new employee is as follows:
```
{
    "lastname": "LASTNAME",
    "secondLastName": "SECONDLASTNAME",
    "firstName": "FIRSTNAME",
    "otherNames": "OTHER NAMES",
    "country": "COLOMBIA",
    "typeOfId": "CITIZEN",
    "idNumber": "Ab232-a30CD",
    "joined": "2022-10-06",
    "area": "INFRASTRUCTURE"
}
```

The basic structure of the json to request changes on an employee is as follows:
```
{
    "lastname": "LASTNAME",
    "secondLastName": "SECONDLASTNAME",
    "firstName": "FIRSTNAME",
    "otherNames": "OTHERNAMES",
    "country": "USA",
    "typeOfId": "SPECIAL_PERMISSION",
    "idNumber": "A-b232a30CD",
    "email": "FIRSTNAME.LASTNAME.2@cidenet.com.us",
    "joined": "2022-10-04",
    "area": "ADMINISTRATION",
    "state": "ACTIVE"
}
```


## Support
If you have any questions, do not hesitate to contact the development team (gfcaneva@gmail.com) with the subject "CIDENET_CHALLENGE".

## DEMO
Google Drives: https://drive.google.com/file/d/1yR2n15EEkFzvCgJLt5TZd4rkHa6QwJMu/view

## Roadmap
In future versions, modifications will be made to the application allowing new functions, separating the information in a complete structure of microservices, deploying it in contanier, implementing CI/CD pipelines and with its subsequent release to production.

## Authors and acknowledgment
@gfocaneva: Caneva, Gianfranco

