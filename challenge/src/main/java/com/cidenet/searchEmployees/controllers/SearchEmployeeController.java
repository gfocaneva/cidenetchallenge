package com.cidenet.searchEmployees.controllers;

import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.providers.SearchEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/")
public class SearchEmployeeController {

    @Autowired
    SearchEmployeeService searchEmployeeService;

    //-------------------------- Get Controllers -----------------------------------

    //---------------------- Get all employees controller --------------------------
    /**
     *
     * @param pageable parameter loaded automatically to inform that the information wants to be paginated
     * @return Returns all objects created in paginated form.
     * Sample: http://localhost:8080/get-all-users/?size=6&page=2
     */
    @GetMapping("/get-all-users")
    public Page<Employee> getAllUsers(@PageableDefault(page = 0, size = 10) Pageable pageable) {
        return (Page<Employee>) searchEmployeeService.getAllEmployees(pageable);
    }

    //---------------------- Get specific employees controller --------------------------
    /**
     * Controller in charge of handling REST requests to search specific employee by email.
     * @param email email by which you want to search for the employee
     * @return Employee object containing all the user information persisted in the database with the given email.
     * Sample: http://localhost:8080/get-an-user/USER.NAME@cidenet.com.us
     */
    @GetMapping(path = "/get-an-user/{email}")
    public Optional<Employee> getSpecificUserByEmail(@PathVariable("email") String email) {

        return searchEmployeeService.getAnEmployee(email);
    }

    //---------------------- Filter Employees by criteria (can be one or many) -------------------------
    /**
     * REST Controller in charge of handling multiple filter requests from the user.
     * It can contain one or multiple search criteria depending on what you want to search for.
     * @param pageable parameter loaded automatically to inform that the information wants to be paginated.
     * @param firstName name containing the user.
     * @param otherNames other names the user may have.
     * @param lastName last user name.
     * @param secondLastName
     * @param typeOfId type of identification held by the entity.
     *                 CITIZEN
     *                 FOREIGN
     *                 PASSPORT
     * @param idNumber number of the national identification document.
     * @param country user's country of residence.
     *                COLOMBIA
     *                USA
     * @param email email by which you want to search
     * @param state status of the entity
     * @return All objects meeting the search criteria, returned in paginated form.
     * Sample: http://localhost:8080/filter?lastName=LASTNAME&state=ACTIVE&country=USA&typeOfId=SPECIAL_PERMISSION
     */
    @GetMapping("/filter")
    public Page<Employee> getUsersByCriteria(
            @PageableDefault(page = 0, size = 10) Pageable pageable,
            @RequestParam(value = "firstName", required = false) String firstName,
            @RequestParam(value = "otherNames", required = false) String otherNames,
            @RequestParam(value = "lastName", required = false) String lastName,
            @RequestParam(value = "secondLastName", required = false) String secondLastName,
            @RequestParam(value = "typeOfId", required = false) String typeOfId,
            @RequestParam(value = "idNumber", required = false) String idNumber,
            @RequestParam(value = "country", required = false) String country,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "state", required = false) String state
    ) {
        return searchEmployeeService.getEmployeesByAllCriteria(
                pageable,
                firstName,
                otherNames,
                lastName,
                secondLastName,
                typeOfId,
                idNumber,
                country,
                email,
                state);
    }
}
