package com.cidenet.searchEmployees.providers;

import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.repositories.SearchEmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SearchEmployeeService {

    Logger logger = LoggerFactory.getLogger(SearchEmployeeService.class);
    @Autowired
    SearchEmployeeRepository searchEmployeeRepository;

    /**
     * Query a repository for all the items in it.
     * @param pageable pageable object with all the required information to return all the employees.
     * @return pageable object with all the employees extracted from the database.
     */
    public Page<Employee> getAllEmployees(Pageable pageable) {
        logger.info("Getting all users. Page size:{}.", pageable.getPageSize());
        return searchEmployeeRepository.findAll(pageable);
    }

    /**
     * Query to repository by a user with a specific email address.
     * @param email specific email that you want to query the database.
     * @return Object returned by the database. Null if there is no match or the object with all its information if there is a match.
     */
    public Optional<Employee> getAnEmployee(String email) {
        logger.info("Getting employee by email:{}.", email);
        return Optional.ofNullable(searchEmployeeRepository.findByEmail(email));
    }


    //---------------------- Filter EmployeesBy_Criteria Services  -------------------------
    /**
     * Function that ensures that all elements contain information to avoid malformation in the data request.
     * @param pageable parameter loaded automatically to inform that the information wants to be paginated.
     * @param firstName name containing the user.
     * @param otherNames other names the user may have.
     * @param lastName last user name.
     * @param secondLastName
     * @param typeOfId type of identification held by the entity.
     *                 CITIZEN
     *                 FOREIGN
     *                 PASSPORT
     * @param idNumber number of the national identification document.
     * @param country user's country of residence.
     *                COLOMBIA
     *                USA
     * @param email email by which you want to search
     * @param state status of the entity
     * @return All objects meeting the search criteria, returned from the repository.
     */
    public Page<Employee> getEmployeesByAllCriteria(
            Pageable pageable,
            String firstName,
            String otherNames,
            String lastName,
            String secondLastName,
            String typeOfId,
            String idNumber,
            String country,
            String email,
            String state) {
        String queryFirstName = guaranteeAnStringParameter(firstName);
        String queryOtherNames = guaranteeAnStringParameter(otherNames);
        String queryLastName = guaranteeAnStringParameter(lastName);
        String querySecondLastName = guaranteeAnStringParameter(secondLastName);
        String queryTypeOfId = guaranteeAnStringParameter(typeOfId);
        String queryIdNumber = guaranteeAnStringParameter(idNumber);
        String queryCountry = guaranteeAnStringParameter(country);
        String queryEmail = guaranteeAnStringParameter(email);
        String queryState = guaranteeAnStringParameter(state);
        logger.info("Searching employees by criteria.");
        return searchEmployeeRepository.filterEmployeesByCriteria(
                pageable,
                queryFirstName,
                queryOtherNames,
                queryLastName,
                querySecondLastName,
                queryTypeOfId,
                queryIdNumber,
                queryCountry,
                queryEmail,
                queryState);
    }

    /**
     * Given a parameter, checks if it is null. If it is, it returns an empty character.
     * @param parameter
     * @return
     */
    private String guaranteeAnStringParameter(String parameter) {
        //Check if an attribute is null. If it is, returns empty string in order to complain to query string
        return parameter == null ? "" : parameter;
    }

}
