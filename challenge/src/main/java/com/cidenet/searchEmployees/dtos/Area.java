package com.cidenet.searchEmployees.dtos;

public enum Area {

    ADMINISTRATION("Administration"),
    FINANCIAL("Financial"),
    PURCHASING("Purchasing"),
    INFRASTRUCTURE("Infrastructure"),
    OPERATION("Operation"),
    HUMAN_TALENT("Human Talent"),
    OTHER_SERVICES("Other Services");

    private String area;

    public String getArea() {
        return area;
    }

    Area(String area) {
        this.area = area;

    }
}
