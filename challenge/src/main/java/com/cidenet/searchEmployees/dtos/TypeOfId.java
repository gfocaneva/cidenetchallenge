package com.cidenet.searchEmployees.dtos;

public enum TypeOfId {
    CITIZEN("Cédula de Ciudadanía"),
    FOREIGN("Cédula de Extranjería"),
    PASSPORT("Pasaporte"),
    SPECIAL_PERMISSION("Permiso Especial");

    private String typeOfId;

    public String getTypeOfId() {
        return typeOfId;
    }

    TypeOfId(String typeOfId) {
        this.typeOfId = typeOfId;

    }

}
