package com.cidenet.searchEmployees.dtos;

public enum Country {

    COLOMBIA("Colombia"),
    USA("United States of America");

    private String country;

    public String getCountry() {
        return country;
    }

    Country(String country) {
        this.country = country;

    }

}
