package com.cidenet.searchEmployees.repositories;

import com.cidenet.searchEmployees.dtos.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface SearchEmployeeRepository extends CrudRepository<Employee, Long>, JpaRepository<Employee, Long> {

    String QUERY = "select * from employee e where " +
            "e.first_name like :firstName% and " +
            "e.other_names like :otherNames% and " +
            "e.last_name like :lastName% and " +
            "e.second_last_name like :secondLastName% and " +
            "e.type_of_id like :typeOfId% and " +
            "e.id_number like :idNumber% and " +
            "e.country like :country% and " +
            "e.email like :email% and " +
            "e.state like :state%";

    //Find requests implementing Native SQL Queries
    @Query(value = QUERY, nativeQuery = true)
    Page<Employee> filterEmployeesByCriteria(
            @PageableDefault Pageable pageable,
            @Param("firstName") String firstName,
            @Param("otherNames") String otherNames,
            @Param("lastName") String lastName,
            @Param("secondLastName") String secondLastName,
            @Param("typeOfId") String typeOfId,
            @Param("idNumber") String idNumber,
            @Param("country") String country,
            @Param("email") String email,
            @Param("state") String state);


    public abstract Employee findByEmail(String email);

    //Return emails that starts in certain way and ends with a particular domain using jpa keywords
    public abstract ArrayList<Employee> findByEmailStartsWithAndEmailEndingWith(String emailName, String domain);


}
