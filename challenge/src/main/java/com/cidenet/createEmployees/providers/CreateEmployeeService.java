package com.cidenet.createEmployees.providers;

import com.cidenet.createEmployees.dtos.EmployeeCreate;
import com.cidenet.createEmployees.mappers.CreateEmployeeMapper;
import com.cidenet.Utils;
import com.cidenet.exceptions.EmployeeException;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.repositories.SearchEmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateEmployeeService {

    Logger logger = LoggerFactory.getLogger(CreateEmployeeService.class);
    @Autowired
    SearchEmployeeRepository searchEmployeeRepository;
    @Autowired
    CreateEmployeeMapper createEmployeeMapper;

    /**
     * Service responsible for creating a new employee in the database previously validating all business rules.
     * @param userNew specialized object for the creation function that holds the information sent by the user.
     * @return new object that has been created and saved in the database.
     */
    public Employee createEmployee(EmployeeCreate userNew) {
        //Validations
        if (userNew.getJoined() != null) {
            Utils.checkDateRange(userNew.getJoined());
        }

        checkNamesPresence(userNew);
        EmployeeCreate formattedNewEmployee = checkStringFormat(userNew);

        //Generates email
        int amountOfSameEmails = Utils.findSameAmountOfEmails(userNew.getFirstName(), userNew.getLastName(), userNew.getCountry(), searchEmployeeRepository);
        String email = Utils.generateEmail(userNew.getFirstName(), userNew.getLastName(), userNew.getCountry(), amountOfSameEmails);

        EmployeeCreate modifiedEmployee = EmployeeCreate.Builder.newBuilder().from(formattedNewEmployee).withEmail(email).build();
        Employee persistantModel = createEmployeeMapper.mapCreateModelToPersistanceModel(modifiedEmployee);
        logger.info("New user successfully created and saved. email:{}.", persistantModel.getEmail());
        return searchEmployeeRepository.save(persistantModel);
    }

    /**
     * Function that groups the calls to the function that validates if these fields are present.
     * @param user object that has the data required by the business
     */
    private void checkNamesPresence(EmployeeCreate user) {
        Utils.validatePresence(user.getLastName());
        Utils.validatePresence(user.getSecondLastName());
        Utils.validatePresence(user.getFirstName());
    }

    /**
     * Given an object prepared to create employees,
     * check that the length and format of certain business parameters are valid.
     * @param user object that has the data that will be validated
     * @return new object with reviewed and formatted values (if needed)
     */
    private EmployeeCreate checkStringFormat(EmployeeCreate user) {
        //Rule validations over String characters input
        //Required fields. Always present

        String formattedLastName = Utils.validateInput(user.getLastName(), 20);
        String formattedSecondLastName = Utils.validateInput(user.getSecondLastName(), 20);
        String formattedFirstName = Utils.validateInput(user.getFirstName(), 20);

        EmployeeCreate.Builder createEmployeeBuilder = EmployeeCreate.Builder.newBuilder().from(user)
                .withLastName(formattedLastName)
                .withSecondLastName(formattedSecondLastName)
                .withFirstName(formattedFirstName);

        if (user.getOtherNames() != null) {
            String formattedOtherNames = Utils.validateInput(user.getOtherNames(), 50, true);
            createEmployeeBuilder.withOtherNames(formattedOtherNames);
        }
        if (user.getIdNumber() != null) {
            Utils.validateAlphanumericCharacters(user.getIdNumber());
            String formattedIdNumber = Utils.checkWordLong(user.getIdNumber(), 20);
            createEmployeeBuilder.withIdNumber(formattedIdNumber);
        }
        return createEmployeeBuilder.build();
    }


}
