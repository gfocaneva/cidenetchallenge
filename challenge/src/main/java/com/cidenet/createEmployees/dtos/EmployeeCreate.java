package com.cidenet.createEmployees.dtos;

import com.cidenet.searchEmployees.dtos.Area;
import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.dtos.TypeOfId;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

@JsonDeserialize(builder = EmployeeCreate.Builder.class)
@JsonPropertyOrder({
        "lastName",
        "secondLastName",
        "firstName",
        "otherNames",
        "country",
        "typeOfId",
        "idNumber",
        "email",
        "joined",
        "area",
        "registerTime",
})
public final class EmployeeCreate {

    private static final String LAST_NAME = "lastName";
    private static final String SECOND_LAST_NAME = "secondLastName";
    private static final String FIRST_NAME = "firstName";
    private static final String OTHER_NAMES = "otherNames";
    private static final String COUNTRY = "Country";
    private static final String TYPE_OF_ID = "typeOfId";
    private static final String ID_NUMBER = "idNumber";
    private static final String EMAIL = "email";
    private static final String JOINED = "joined";
    private static final String AREA = "area";
    private static final String STATE = "state";


    private String lastName;
    private String secondLastName;
    private String firstName;
    private String otherNames;
    private Country country;
    private TypeOfId typeOfId;
    private String idNumber;
    private String email;
    private Date joined;
    private Area area;
    private String state = "ACTIVE";

    private EmployeeCreate() {
    }

    // Getters
    //If the name of the json is equal to the property, JsonDeserialize detect the attribute directly. If not, we need to specify @JsonProperty("value")

    public String getLastName() {
        return lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public Country getCountry() {
        return country;
    }

    public TypeOfId getTypeOfId() {
        return typeOfId;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public String getEmail() {
        return email;
    }

    public Date getJoined() {
        return joined;
    }

    public Area getArea() {
        return area;
    }

    public String getState() {
        return state;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        EmployeeCreate that = (EmployeeCreate) o;

        return new EqualsBuilder()
                .append(lastName, that.lastName)
                .append(secondLastName, that.secondLastName)
                .append(firstName, that.firstName)
                .append(otherNames, that.otherNames)
                .append(country, that.country)
                .append(typeOfId, that.typeOfId)
                .append(idNumber, that.idNumber)
                .append(email, that.email)
                .append(joined, that.joined)
                .append(area, that.area)
                .append(state, that.state)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(lastName)
                .append(secondLastName)
                .append(firstName)
                .append(otherNames)
                .append(country)
                .append(typeOfId)
                .append(idNumber)
                .append(email)
                .append(joined)
                .append(area)
                .append(state)
                .toHashCode();
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append(LAST_NAME, lastName)
                .append(SECOND_LAST_NAME, secondLastName)
                .append(FIRST_NAME, firstName)
                .append(OTHER_NAMES, otherNames)
                .append(COUNTRY, country)
                .append(TYPE_OF_ID, typeOfId)
                .append(ID_NUMBER, idNumber)
                .append(EMAIL, email)
                .append(JOINED, joined)
                .append(AREA, area)
                .append(STATE, state)
                .toString();
    }

    @JsonPOJOBuilder
    public static final class Builder {
        private String lastName;
        private String secondLastName;
        private String firstName;
        private String otherNames;
        private Country country;
        private TypeOfId typeOfId;
        private String idNumber;
        private String email;
        private Date joined;
        private Area area;

        private Builder() {
        }

        public Builder from(final EmployeeCreate base) {
            lastName = base.getLastName();
            secondLastName = base.getSecondLastName();
            firstName = base.getFirstName();
            otherNames = base.getOtherNames();
            country = base.getCountry();
            typeOfId = base.getTypeOfId();
            idNumber = base.getIdNumber();
            email = base.getEmail();
            joined = base.getJoined();
            area = base.getArea();
            return this;
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        //Same comment than above with @JsonProperty
        public Builder withLastName(final String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withSecondLastName(final String secondLastName) {
            this.secondLastName = secondLastName;
            return this;
        }


        public Builder withFirstName(final String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withOtherNames(final String otherNames) {
            this.otherNames = otherNames;
            return this;
        }

        public Builder withCountry(final Country country) {
            this.country = country;
            return this;
        }

        public Builder withTypeOfId(final TypeOfId typeOfId) {
            this.typeOfId = typeOfId;
            return this;
        }

        public Builder withIdNumber(final String idNumber) {
            this.idNumber = idNumber;
            return this;
        }

        public Builder withEmail(final String email) {
            this.email = email;
            return this;
        }

        public Builder withJoined(final Date joined) {
            this.joined = joined;
            return this;
        }

        public Builder withArea(final Area area) {
            this.area = area;
            return this;
        }


        public EmployeeCreate build() {
            final EmployeeCreate instance = new EmployeeCreate();
            instance.lastName = lastName;
            instance.secondLastName = secondLastName;
            instance.firstName = firstName;
            instance.otherNames = otherNames;
            instance.country = country;
            instance.typeOfId = typeOfId;
            instance.idNumber = idNumber;
            instance.email = email;
            instance.joined = joined;
            instance.area = area;
            return instance;
        }

    }

}







