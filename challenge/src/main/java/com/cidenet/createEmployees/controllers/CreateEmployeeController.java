package com.cidenet.createEmployees.controllers;

import com.cidenet.createEmployees.dtos.EmployeeCreate;
import com.cidenet.createEmployees.providers.CreateEmployeeService;
import com.cidenet.searchEmployees.dtos.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateEmployeeController {

    @Autowired
    CreateEmployeeService createEmployeeService;

    //---------------------- Create a new employee Controller ---------------------------
    /**
     * Controller in charge of handling REST requests to create a new Employee.
     * @param user Required information for user creation.
     * @return Employee returned after creation.
     * Sample: http://localhost:8080/create-user
     */
    @PostMapping("/create-user")
    public Employee createNewEmployee(@RequestBody EmployeeCreate user) {
        return createEmployeeService.createEmployee(user);
    }
}
