package com.cidenet.createEmployees.mappers;

import com.cidenet.createEmployees.dtos.EmployeeCreate;
import com.cidenet.searchEmployees.dtos.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class CreateEmployeeMapper {

    @Autowired
    public CreateEmployeeMapper() {
    }

    /**
     * Function responsible for converting the creation employee input model
     * into the model to be persisted in the database.
     * @param model Source model to be converted
     * @return Object created based on the inputs + extra information, compatible with the database.
     */
    public Employee mapCreateModelToPersistanceModel(final EmployeeCreate model) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String formattedRegisterTime = now.format(formatter);

        return Employee.Builder
                .newBuilder()
                .withLastName(model.getLastName())
                .withSecondLastName(model.getSecondLastName())
                .withFirstName(model.getFirstName())
                .withOtherNames(model.getOtherNames())
                .withCountry(model.getCountry())
                .withTypeOfId(model.getTypeOfId())
                .withIdNumber(model.getIdNumber())
                .withEmail(model.getEmail())
                .withJoined(model.getJoined())
                .withArea(model.getArea())
                .withState(model.getState())
                .withRegisterTime(formattedRegisterTime)
                .build();
    }

}
