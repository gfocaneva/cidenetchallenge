package com.cidenet;

import com.cidenet.exceptions.EmployeeException;
import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.repositories.SearchEmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {
    static Logger logger = LoggerFactory.getLogger(Utils.class);

    /**
     * Validates that the word only contains letters and no accents allowed. If not raise an exception.
     * @param word word to be validated.
     * @param spaceAllowed flag that allows spaces to be included as enabled characters (false by default).
     * @throws EmployeeException raised when not the word does not meet the pattern.
     */
    public static void validateAlphabeticCharacters(String word, Boolean... spaceAllowed) {
        boolean defaultSpaceAllowed = spaceAllowed.length > 0 ? spaceAllowed[0] : false;
        String pattern;
        if (defaultSpaceAllowed) {
            pattern = "[A-Z ]+";
        } else {
            pattern = "[A-Z]+";
        }
        if (!word.matches(pattern)) {
            logger.error("Error. Attempt to save an invalid word:{}.Pattern:{}. Space allowed:{}.", word, pattern, defaultSpaceAllowed);
            throw new EmployeeException("Characters does not follow the pattern:" + pattern);
        }
    }

    /**
     * Validates that the word only contains letters and no accents allowed. If not raise an exception
     * @param characters word to be validated.
     * @throws EmployeeException raised when not the word does not meet the pattern.
     */
    public static void validateAlphanumericCharacters(String characters) {
        //
        String pattern = "^[-a-zA-Z0-9]+";
        if (!characters.matches(pattern)) {
            logger.error("Error. Attempt to save an invalid word:{}.Pattern:{}. Space allowed:{}.", characters, pattern);
            throw new EmployeeException("Character does not follow the pattern.");
        }
    }

    /**
     * Function that checks if the word entered does not exceed a given maximum number. If it does, it modifies it by shortening it to the maximum allowed.
     * @param word word to be validated.
     * @param maxLong maximum number allowed for the word.
     * @return formatted word that complies with the given length.
     */
    public static String checkWordLong(String word, int maxLong) {
        //Check that the amount of characters does not exceed the given limit(N). If it does, it takes the first N elements
        String wordWithoutSpaces = word.trim();
        String correctWordLength = wordWithoutSpaces.length() > maxLong ? wordWithoutSpaces.substring(0, maxLong) : wordWithoutSpaces;
        return correctWordLength;
    }

    /**
     * Function that groups two validations for a word that meets a certain pattern and has a maximum number of characters.
     * @param word word to be validated.
     * @param maxLong word to be validated.
     * @param spaceAllowed spaceAllowed flag that allows spaces to be included as enabled characters (false by default).
     * @return formatted word that complies with the correct format and the  given length.
     */
    public static String validateInput(String word, int maxLong, Boolean... spaceAllowed) {
        //Specific method that aggregates commonly used rules required by employee input information.
        validateAlphabeticCharacters(word, spaceAllowed);
        return checkWordLong(word, maxLong);
    }

    /**
     * Verifies that a required parameter is not null
     * @param attribute given attribute.
     * @throws EmployeeException raised when not the attribute is null.
     */
    public static void validatePresence(Object attribute) {
        if (attribute == null) {
            logger.error("Error. Missing required attributes.");
            throw new EmployeeException("Error. Please, complete all the required information.");
        }
    }

    /**
     * Function that checks if the date entered is not more than 30 days old or in the future.
     * @param date date that you wish to validate.
     * @throws EmployeeException raised when not the date is more than 30 days or in the future.
     */
    public static void checkDateRange(Date date) {
        //Check if the date is not in future and within a month
        int daysBefore = -31;
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, daysBefore);
        Date today30 = cal.getTime();
        if (date.before(today30)) {
            logger.error("Error. Date exceed 30 days. Date:{}.", date.toString());
            throw new EmployeeException("Error. Joined date cannot exceed 30 days from today");
        }
        if (date.after(today)) {
            logger.error("Error. Date is in future. Date:{}.", date.toString());
            throw new EmployeeException("Error. Joined date cannot be in the future");
        }
    }

    /**
     * Creates an email combining first name and last name and adds ID (depending on how many same emails exists).
     * @param firstName first name of the person who is to generate the mailing.
     * @param lastName lastname of the person who is to generate the mailing.
     * @param country user's country of residence.
     *                COLOMBIA
     *                USA
     * @param previousAmountOfSameNameAndLastName number of emails with the same FIRST_NAME and LAST_NAME previously created.
     * @return email created with the following structure: <FIRST_NAME>.<LAST_NAME>.<ID>@<DOMAIN>
     */
    public static String generateEmail(String firstName, String lastName, Country country,
                                       int previousAmountOfSameNameAndLastName) {
        //Generates automatic email domain and given

        String finalEmail = "";

        String emailPlaceHolder = generatesEmailPlaceHolder(firstName, lastName, country);
        String[] splitEmailStructure = emailPlaceHolder.split("@");
        if (previousAmountOfSameNameAndLastName > 0) {
            //The plain email has been taken and needs to calculate and add an incremental id
            int id = previousAmountOfSameNameAndLastName + 1;
            finalEmail = splitEmailStructure[0].concat(".").concat(String.valueOf(id)).concat("@").concat(splitEmailStructure[1]);
        } else {
            //There is no previous email with this string
            finalEmail = splitEmailStructure[0].concat("@").concat(splitEmailStructure[1]);
        }
        logger.info("Generating new email:{}", finalEmail);
        return finalEmail;
    }

    /**
     * Creates an email, without ID, combining first name and last name.
     * @param firstName first name of the person who is to generate the mailing.
     * @param lastname lastname of the person who is to generate the mailing.
     * @param country user's country of residence.
     *                COLOMBIA (default)
     *                USA
     * @return email created with the following structure: <FIRST_NAME>.<LAST_NAME>@<DOMAIN>
     */
    static String generatesEmailPlaceHolder(String firstName, String lastname, Country country) {
        //Checks country. If it is null, selects United States
        String nationality = country == Country.COLOMBIA ? "cidenet.com.co" : "cidenet.com.us";

        String emailName = firstName.concat(".").concat(lastname);
        String domain = "@".concat(nationality);
        return emailName.concat(domain);
    }

    /**
     * Counts how many emails are created with the same name and domain (without id, in order to assign the last id
     * to the next email).
     * @param firstName first name of the person who is to generate the mailing.
     * @param lastName lastname of the person who is to generate the mailing.
     * @param country user's country of residence.
     *                COLOMBIA
     *                USA
     * @param searchEmployeeRepository repository entity in which you want to search for matches.
     * @return number of email matches.
     */
    public static int findSameAmountOfEmails(String firstName, String lastName, Country country, SearchEmployeeRepository
            searchEmployeeRepository) {
        String email = generatesEmailPlaceHolder(firstName, lastName, country);
        String[] splitEmailStructure = email.split("@");
        ArrayList<Employee> amountOfSameEmail;
        amountOfSameEmail = searchEmployeeRepository.findByEmailStartsWithAndEmailEndingWith(splitEmailStructure[0], splitEmailStructure[1]);
        return amountOfSameEmail.size();
    }

}




