package com.cidenet.editEmployees.mappers;

import com.cidenet.editEmployees.dtos.EmployeeEdit;
import com.cidenet.searchEmployees.dtos.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class EditEmployeeMapper {

    @Autowired
    public EditEmployeeMapper() {
    }

    /**
     *  Function responsible for taking the changes applied to an employee,
     *  consolidating them and converting them into a valid object to be stored.
     * @param model Object with the information that will replace the existing employee.
     * @param storedEmployee Object previously stored in the database which is to be replaced.
     * @return Employed with the necessary information to be persistent.
     */
    public Employee mapEditModelToPersistanceModel(final EmployeeEdit model, Employee storedEmployee) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String formattedRegisterTime = now.format(formatter);

        return Employee.Builder
                .newBuilder()
                .withId(storedEmployee.getId())
                .withLastName(model.getLastName())
                .withSecondLastName(model.getSecondLastName())
                .withFirstName(model.getFirstName())
                .withOtherNames(model.getOtherNames())
                .withCountry(model.getCountry())
                .withTypeOfId(model.getTypeOfId())
                .withIdNumber(model.getIdNumber())
                .withEmail(model.getEmail())
                .withJoined(model.getJoined())
                .withArea(model.getArea())
                .withState(model.getState())
                .withRegisterTime(storedEmployee.getRegisterTime())
                .withLastEditTime(formattedRegisterTime)
                .build();
    }

}
