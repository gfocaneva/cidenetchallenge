package com.cidenet.editEmployees.controllers;

import com.cidenet.editEmployees.dtos.EmployeeEdit;
import com.cidenet.editEmployees.providers.EditEmployeeService;
import com.cidenet.searchEmployees.dtos.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EditEmployeeController {

    @Autowired
    EditEmployeeService editEmployeeService;

    //------------------------- Edit an employee Controller -----------------------------
    /**
     * Controller in charge of handling REST requests to edit a new Employee.
     * @param user Required information for user edition.
     * @return Employee with all edits applied
     * Sample: http://localhost:8080/edit-user
     */
    @PostMapping("/edit-user")
    public Employee editEmployee(@RequestBody EmployeeEdit user) {
        return editEmployeeService.editEmployee(user);
    }
}
