package com.cidenet.editEmployees.providers;

import com.cidenet.editEmployees.dtos.EmployeeEdit;
import com.cidenet.editEmployees.mappers.EditEmployeeMapper;
import com.cidenet.Utils;
import com.cidenet.exceptions.EmployeeException;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.repositories.SearchEmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EditEmployeeService {

    Logger logger = LoggerFactory.getLogger(EditEmployeeService.class);
    @Autowired
    SearchEmployeeRepository searchEmployeeRepository;
    @Autowired
    EditEmployeeMapper editEmployeeMapper;

    /**
     * Service responsible for overwriting the information of an existing user in the database
     * by validating different business rules.
     * @param userEdited object containing the information to overwrite the employee
     * @return object that has been modified and saved in the database.
     * @throws EmployeeException when cannot load user from the database.
     */
    public Employee editEmployee(EmployeeEdit userEdited) {
        //Check validations for edition
        if (userEdited.getJoined() != null) {
            Utils.checkDateRange(userEdited.getJoined());
        }
        checkNamesPresence(userEdited);
        EmployeeEdit formattedEditEmployee = checkStringFormat(userEdited);

        //Load stored user
        Employee storedEmployee;
        try {
            storedEmployee = Optional.ofNullable(searchEmployeeRepository.findByEmail(userEdited.getEmail())).orElseThrow(() -> new Exception("Employee not found - " + userEdited.getEmail()));
        } catch (Exception e) {
            logger.error("Error loading employee with email:{}", userEdited.getEmail());
            throw new EmployeeException("Problems trying to locate the user on the database. Please check the information or try later.");
        }

        //Validate email is correct
        String email = verificationOfNewEmail(userEdited, storedEmployee);

        EmployeeEdit modifiedEmployee = EmployeeEdit.Builder.newBuilder()
                .from(formattedEditEmployee)
                .withEmail(email)
                .build();

        Employee persistantModel = editEmployeeMapper.mapEditModelToPersistanceModel(modifiedEmployee, storedEmployee);
        logger.info("Employee information updated successfully and saved. id:{} email:{} ", persistantModel.getId(), persistantModel.getEmail());
        return searchEmployeeRepository.save(persistantModel);
    }

    /**
     * Function that checks if some the email components (firstName or lastName) have changed.
     * If yes, generates a new email with the new email components. if not, it returns the same email that was saved.
     * @param userEdited object with the information to be replaced.
     * @param storedEmployee object that is currently stored in the database.
     * @return email to be assigned to the employee.
     */
    private String verificationOfNewEmail(EmployeeEdit userEdited, Employee storedEmployee) {
        String email = storedEmployee.getEmail();
        if (!(userEdited.getFirstName().equals(storedEmployee.getFirstName())) ||
                (!userEdited.getLastName().equals(storedEmployee.getLastName()))) {
            String oldEmail = email;
            int amountOfSameEmails = Utils.findSameAmountOfEmails(userEdited.getFirstName(), userEdited.getLastName(), userEdited.getCountry(), searchEmployeeRepository);
            email = Utils.generateEmail(userEdited.getFirstName(), userEdited.getLastName(), storedEmployee.getCountry(), amountOfSameEmails);
            logger.info("Replacing email from {}(old) to {}(new).", oldEmail, email);
        }
        return email;
    }

    /**
     * Function that groups the calls to the function that validates if certain fields are present.
     * @param user object that has the data required by the business
     */
    private void checkNamesPresence(EmployeeEdit user) {
        //Rules can be different for editing and for creating. That's why this method is not in utils
        Utils.validatePresence(user.getLastName());
        Utils.validatePresence(user.getSecondLastName());
        Utils.validatePresence(user.getFirstName());
    }

    /**
     * Given an object prepared to edit employees,
     * check that the length and format of certain business parameters are valid.
     * @param user object that has the data that will be validated
     * @return new object with reviewed and formatted values (if needed)
     */
    private EmployeeEdit checkStringFormat(EmployeeEdit user) {
        //Required fields. Always present
        String formattedLastName = Utils.validateInput(user.getLastName(), 20);
        String formattedSecondLastName = Utils.validateInput(user.getSecondLastName(), 20);
        String formattedFirstName = Utils.validateInput(user.getFirstName(), 20);

        EmployeeEdit.Builder editEmployeeBuilder = EmployeeEdit.Builder.newBuilder().from(user)
                .withLastName(formattedLastName)
                .withSecondLastName(formattedSecondLastName)
                .withFirstName(formattedFirstName);

        if (user.getOtherNames() != null) {
            String formattedOtherNames = Utils.validateInput(user.getOtherNames(), 50, true);
            editEmployeeBuilder.withOtherNames(formattedOtherNames);
        }
        if (user.getIdNumber() != null) {
            Utils.validateAlphanumericCharacters(user.getIdNumber());
            String formattedIdNumber = Utils.checkWordLong(user.getIdNumber(), 20);
            editEmployeeBuilder.withIdNumber(formattedIdNumber);
        }

        return editEmployeeBuilder.build();
    }

}
