package com.cidenet;

import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.dtos.TypeOfId;

public class TestUtils {

    public static final String FIRST_NAME = "FIRSTNAME";
    public static final String LASTNAME = "LASTNAME";
    public static final String SECOND_LASTNAME = "SECONDLASTNAME";
    public static final String EMAIL = "FIRSTNAME.LASTNAME@cidenet.com.co";
    public static final String OTHER_NAMES = "OTHER NAMES";
    public static final String ID_DOCUMENT = "ABC123";
    public static final String STATE = "ACTIVE";


    public static Employee getDummyStoredEmployee() {
        //Information stored in the Database
        return Employee.Builder
                .newBuilder()
                .withId(1L)
                .withLastName(LASTNAME)
                .withSecondLastName(SECOND_LASTNAME)
                .withFirstName(FIRST_NAME)
                .withOtherNames(OTHER_NAMES)
                .withCountry(Country.COLOMBIA)
                .withTypeOfId(TypeOfId.SPECIAL_PERMISSION)
                .withIdNumber(ID_DOCUMENT)
                .withEmail(EMAIL)
                .withJoined(null)
                .withArea(null)
                .withState(null)
                .withRegisterTime(null)
                .withLastEditTime(null)
                .build();
    }
}
