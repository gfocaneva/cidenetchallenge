package com.cidenet.searchEmployees;

import com.cidenet.Utils;
import com.cidenet.exceptions.EmployeeException;
import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.repositories.SearchEmployeeRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UtilsTest {

    public static final int MAX_LONG = 10;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Mock
    SearchEmployeeRepository searchEmployeeRepository;

    @Test
    public void validateAlphabeticCharactersOnlyAcceptsCapitalLetters() {
        String word = "TESTWORD";
        Utils.validateAlphabeticCharacters(word);
    }

    @Test
    public void validateAlphabeticCharactersAcceptsCapitalLettersAndSpaces() {
        String word = "TEST WORD";
        Utils.validateAlphabeticCharacters(word, true);
    }

    @Test(expected = EmployeeException.class)
    public void validateAlphabeticCharactersThrowsErrorWhenFormatContainsSpace() {
        String word = "V A L I D FORMAT";
        Utils.validateAlphabeticCharacters(word, false);
    }

    @Test
    public void validateAlphabeticCharactersThrowsErrorWhenContainsLowerLetters() {
        String word = "InvalidWord";
        //Other way to test error message rather than annotation
        EmployeeException employeeException = assertThrows(EmployeeException.class, () -> Utils.validateAlphabeticCharacters(word, false));
        assertTrue(employeeException.getMessage().contains("Characters does not follow the pattern:[A-Z]+"));
    }

    @Test
    public void validateAlphanumericCharactersExecutesOk() {
        String word = "Test-W1O2R3D";
        Utils.validateAlphanumericCharacters(word);
    }

    @Test(expected = EmployeeException.class)
    public void validateAlphanumericCharactersThrowsErrorWhenFormatContainsSpace() {
        String word = "Inv a lid-W1O2R3D";
        Utils.validateAlphanumericCharacters(word);
    }

    @Test
    public void checkWordLongWorksOk() {
        String expectedResult = "1234567891";
        String input = "1234567891";
        String result = Utils.checkWordLong(input, MAX_LONG);
        assertEquals(result, expectedResult);
    }

    @Test
    public void checkWordLongWorksReceivesMoreCharactersThanLimit() {
        String expectedResult = "1234567891";
        String input = " 123456789123456788998     ";
        String result = Utils.checkWordLong(input, MAX_LONG);
        assertEquals(result, expectedResult);
    }

    @Test
    public void validateInputWorksFine() {
        String expectedResult = "SOME WORD";
        String word = " SOME WORD ";
        boolean spaceAllowed = true;
        String result = Utils.validateInput(word, MAX_LONG, spaceAllowed);
        assertEquals(result, expectedResult);
    }

    @Test
    public void validatePresenceWorksFine() {
        Utils.validatePresence("Something");
    }

    @Test(expected = EmployeeException.class)
    public void validatePresenceThrowsAnException() {
        Utils.validatePresence(null);
    }

    @Test
    public void checkDateRangeIsWithinRange() {
        Date date = new Date(System.currentTimeMillis());
        Utils.checkDateRange(date);
    }

    @Test(expected = EmployeeException.class)
    public void checkDateRangeIsMoreThan30Days() {
        Date date = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
        Utils.checkDateRange(date);
    }

    @Test
    public void checkDateRangeIsInFuture() {
        Date date = new GregorianCalendar(2050, Calendar.FEBRUARY, 11).getTime();
        //Other way to test error message rather than annotation
        EmployeeException employeeException = assertThrows(EmployeeException.class, () -> Utils.checkDateRange(date));
        assertTrue(employeeException.getMessage().contains("Error. Joined date cannot be in the future"));

    }

    @Test
    public void generateEmail() {
        String expectedResult = "firstName.lastName.2@cidenet.com.co";
        String firstName = "firstName";
        String lastName = "lastName";
        String result = Utils.generateEmail(firstName, lastName, Country.COLOMBIA, 1);
        assertEquals(result, expectedResult);
    }

    @Test
    public void findSameAmountOfEmailsCallsRepositoryProperly() {
        String emailName = "firstName.lastName";
        String firstName = "firstName";
        String lastName = "lastName";
        String domain = "cidenet.com.us";
        //if country is null it will take us as default
        Utils.findSameAmountOfEmails(firstName, lastName, null, searchEmployeeRepository);
        verify(searchEmployeeRepository, times(1)).findByEmailStartsWithAndEmailEndingWith(emailName, domain);
    }
}