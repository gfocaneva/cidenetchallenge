package com.cidenet.createEmployees;

import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.dtos.TypeOfId;
import com.cidenet.createEmployees.dtos.EmployeeCreate;

public class TestUtilCreateEmployee {

    public static final String FIRST_NAME = "FIRSTNAME";
    public static final String LASTNAME = "LASTNAME";
    public static final String SECOND_LASTNAME = "SECONDLASTNAME";
    public static final String EMAIL = "FIRSTNAME.LASTNAME@cidenet.com.co";
    public static final String ID_DOCUMENT = "ABC123";
    public static final String STATE = "ACTIVE";


    public static EmployeeCreate getDummyCreatedEmployee() {
        //Information to be created
        return EmployeeCreate.Builder.newBuilder()
                .withFirstName(FIRST_NAME)
                .withLastName(LASTNAME)
                .withSecondLastName(SECOND_LASTNAME)
                .withCountry(Country.COLOMBIA)
                .withTypeOfId(TypeOfId.SPECIAL_PERMISSION)
                .withIdNumber(ID_DOCUMENT)
                .withEmail(EMAIL)
                .build();
    }
}
