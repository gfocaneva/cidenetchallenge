package com.cidenet.createEmployees.providers;

import com.cidenet.TestUtils;
import com.cidenet.createEmployees.TestUtilCreateEmployee;
import com.cidenet.createEmployees.dtos.EmployeeCreate;
import com.cidenet.createEmployees.mappers.CreateEmployeeMapper;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.repositories.SearchEmployeeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CreateSearchEmployeeServiceTest {
    @Mock
    private SearchEmployeeRepository searchEmployeeRepository;
    @Mock
    private CreateEmployeeMapper createEmployeeMapper;
    @InjectMocks
    private CreateEmployeeService classUnderTest = new CreateEmployeeService();

    @Test
    public void createEmployee() {
        Mockito.doReturn(TestUtils.getDummyStoredEmployee()).when(createEmployeeMapper).mapCreateModelToPersistanceModel(any(EmployeeCreate.class));
        Mockito.doReturn(TestUtils.getDummyStoredEmployee()).when(searchEmployeeRepository).save(any(Employee.class));

        Employee employee = classUnderTest.createEmployee(TestUtilCreateEmployee.getDummyCreatedEmployee());

        verify(createEmployeeMapper, times(1)).mapCreateModelToPersistanceModel(any(EmployeeCreate.class));
        verify(searchEmployeeRepository, times(1)).save(any(Employee.class));
    }
}