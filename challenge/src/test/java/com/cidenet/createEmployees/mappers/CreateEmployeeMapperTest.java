package com.cidenet.createEmployees.mappers;

import com.cidenet.createEmployees.TestUtilCreateEmployee;
import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.dtos.TypeOfId;
import org.junit.Test;

import static com.cidenet.TestUtils.*;
import static org.junit.Assert.*;

public class CreateEmployeeMapperTest {
    private CreateEmployeeMapper classUnderTests = new CreateEmployeeMapper();

    @Test
    public void mapCreateModelToPersistanceModel() {
        Employee result = classUnderTests.mapCreateModelToPersistanceModel(TestUtilCreateEmployee.getDummyCreatedEmployee());

        //Result is expected type
        assertTrue(Employee.class.isInstance(result));

        assertEquals(result.getLastName(), LASTNAME);
        assertEquals(result.getSecondLastName(), SECOND_LASTNAME);
        assertEquals(result.getFirstName(), FIRST_NAME);

        //Removing Other names param
        assertEquals(result.getOtherNames(), null);

        assertEquals(result.getCountry(), Country.COLOMBIA);
        assertEquals(result.getTypeOfId(), TypeOfId.SPECIAL_PERMISSION);
        assertEquals(result.getIdNumber(), ID_DOCUMENT);
        assertEquals(result.getEmail(), EMAIL);
        assertNull(result.getJoined());
        assertNull(result.getArea());

        //State is always "ACTIVE" when it creates
        assertEquals(result.getState(), STATE);

        assertNotNull(result.getRegisterTime());

    }
}