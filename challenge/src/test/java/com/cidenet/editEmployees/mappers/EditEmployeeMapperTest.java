package com.cidenet.editEmployees.mappers;

import com.cidenet.TestUtils;
import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.dtos.TypeOfId;
import com.cidenet.editEmployees.TestUtilEditEmployee;
import org.junit.Test;

import static com.cidenet.editEmployees.TestUtilEditEmployee.*;
import static org.junit.Assert.*;

public class EditEmployeeMapperTest {
    private EditEmployeeMapper classUnderTests = new EditEmployeeMapper();

    @Test
    public void mapEditModelToPersistanceModel() {

        Employee result = classUnderTests.mapEditModelToPersistanceModel(TestUtilEditEmployee.getDummyEdittedEmployee(), TestUtils.getDummyStoredEmployee());

        //Result is expected type
        assertTrue(Employee.class.isInstance(result));

        assertEquals(result.getLastName(), EDITED_LASTNAME);
        assertEquals(result.getSecondLastName(), EDITED_SECOND_LASTNAME);
        assertEquals(result.getFirstName(), EDITED_FIRST_NAME);

        //Removing Other names param
        assertEquals(result.getOtherNames(), null);

        assertEquals(result.getCountry(), Country.COLOMBIA);
        assertEquals(result.getTypeOfId(), TypeOfId.SPECIAL_PERMISSION);
        assertEquals(result.getIdNumber(), EDITED_ID_DOCUMENT);
        assertEquals(result.getEmail(), EMAIL);
        assertNull(result.getJoined());
        assertNull(result.getArea());

        //State was null, now is not
        assertEquals(result.getState(), STATE);

        assertNull(result.getRegisterTime());
        assertNotNull(result.getLastEditTime());

    }

}
