package com.cidenet.editEmployees.providers;


import com.cidenet.TestUtils;
import com.cidenet.searchEmployees.dtos.Employee;
import com.cidenet.searchEmployees.repositories.SearchEmployeeRepository;
import com.cidenet.editEmployees.TestUtilEditEmployee;
import com.cidenet.editEmployees.dtos.EmployeeEdit;
import com.cidenet.editEmployees.mappers.EditEmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EditSearchEmployeeServiceTest {

    @Mock
    private SearchEmployeeRepository searchEmployeeRepository;
    @Mock
    private EditEmployeeMapper editEmployeeMapper;
    @InjectMocks
    private EditEmployeeService classUnderTest = new EditEmployeeService();

    @Test
    public void checkNamesPresenceCallsActualMethodValidator() {
        Mockito.doReturn(TestUtils.getDummyStoredEmployee()).when(searchEmployeeRepository).findByEmail(Mockito.anyString());
        Mockito.doReturn(TestUtils.getDummyStoredEmployee()).when(editEmployeeMapper).mapEditModelToPersistanceModel(any(EmployeeEdit.class), any(Employee.class));

        Mockito.doReturn(TestUtils.getDummyStoredEmployee()).when(searchEmployeeRepository).save(any(Employee.class));

        Employee employee = classUnderTest.editEmployee(TestUtilEditEmployee.getDummyEdittedEmployee());

        verify(searchEmployeeRepository, times(1)).findByEmail(TestUtilEditEmployee.getDummyEdittedEmployee().getEmail());
        verify(editEmployeeMapper, times(1)).mapEditModelToPersistanceModel(any(EmployeeEdit.class),any(Employee.class));
        verify(searchEmployeeRepository, times(1)).save(any(Employee.class));
    }

}