package com.cidenet.editEmployees;

import com.cidenet.editEmployees.dtos.EmployeeEdit;
import com.cidenet.searchEmployees.dtos.Country;
import com.cidenet.searchEmployees.dtos.TypeOfId;

public class TestUtilEditEmployee {

    public static final String EMAIL = "FIRSTNAME.LASTNAME@cidenet.com.co";
    public static final String STATE = "ACTIVE";

    public static final String EDITED_FIRST_NAME = "EDITEDFIRSTNAME";
    public static final String EDITED_LASTNAME = "EDITEDLASTNAME";
    public static final String EDITED_SECOND_LASTNAME = "EDITEDSECONDLASTNAME";
    public static final String EDITED_ID_DOCUMENT = "XYZ789";

    public static EmployeeEdit getDummyEdittedEmployee() {
        //Information to be updated
        return EmployeeEdit.Builder.newBuilder()
                .withFirstName(EDITED_FIRST_NAME)
                .withLastName(EDITED_LASTNAME)
                .withSecondLastName(EDITED_SECOND_LASTNAME)
                .withCountry(Country.COLOMBIA)
                .withTypeOfId(TypeOfId.SPECIAL_PERMISSION)
                .withIdNumber(EDITED_ID_DOCUMENT)
                .withEmail(EMAIL)
                .withState(STATE)
                .build();
    }


}
